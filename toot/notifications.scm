;;; Toot --- Mastodon client.
;;; Copyright © 2020 Masaya Tojo <masaya@tojo.tokyo>
;;;
;;; This file is part of Toot.
;;;
;;; Toot is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; Toot is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Toot.  If not, see <http://www.gnu.org/licenses/>.

(define-module (toot notifications)
  #:use-module (toot statuses)
  #:use-module (toot accounts)
  #:use-module (toot utils)
  #:use-module (srfi srfi-9)
  #:export (notification-from-json
            notification?
            notification-id
            notification-status
            notification-account
            notification-type
            notification-creation-time))

(define-record-type <notification>
  (notification-from-json json)
  notification?
  (json notification-json))

(define (notification-id notification)
  (assoc-ref (notification-json notification)
             "id"))

(define (notification-status notification)
  (status-from-json
   (assoc-ref (notification-json notification)
              "status")))

(define (notification-account notification)
  (account-from-json
   (assoc-ref (notification-json notification)
              "account")))

(define (notification-type notification)
  (string->symbol
   (assoc-ref (notification-json notification)
              "type")))

(define (notification-creation-time notification)
  (created-at->creation-time
   (assoc-ref (notification-json notification)
              "created_at")))
