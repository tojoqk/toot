;;; Toot --- Mastodon client.
;;; Copyright © 2020 Masaya Tojo <masaya@tojo.tokyo>
;;;
;;; This file is part of Toot.
;;;
;;; Toot is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; Toot is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Toot.  If not, see <http://www.gnu.org/licenses/>.

(define-module (toot emojis)
  #:use-module (srfi srfi-9)
  #:use-module (srfi srfi-9 gnu)
  #:use-module (srfi srfi-1)
  #:use-module (ice-9 regex)
  #:export (emoji-from-json
            emoji?
            emoji-static-url
            emoji-shortcode
            emoji-visible-in-picker?
            emojis->regexp
            emoji-find))

(define-record-type <emoji>
  (emoji-from-json json)
  emoji?
  (json emoji-json))

(define (emoji-static-url emoji)
  (assoc-ref (emoji-json emoji) "static_url"))

(define (emoji-shortcode emoji)
  (assoc-ref (emoji-json emoji) "shortcode"))

(define (emoji-visible-in-picker? emoji)
  (assoc-ref (emoji-json emoji) "visible_in_picker"))

(define (emojis->regexp emojis)
  (string-append ":("
                 (string-join (map (compose regexp-quote emoji-shortcode)
                                   emojis)
                              "|")
                 ":)"))

(define (emoji-find emojis shortcode)
  (find (lambda (emoji) (equal? shortcode (emoji-shortcode emoji)))
        emojis))
