;;; Toot --- Mastodon client.
;;; Copyright © 2020 Masaya Tojo <masaya@tojo.tokyo>
;;;
;;; This file is part of Toot.
;;;
;;; Toot is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; Toot is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Toot.  If not, see <http://www.gnu.org/licenses/>.

(define-module (toot accounts)
  #:use-module (toot emojis)
  #:use-module (srfi srfi-9)
  #:use-module (srfi srfi-9 gnu)
  #:use-module (ice-9 format)
  #:export (account-from-json
            account?
            account-id
            account-display-name
            account-avatar-static
            account-acct
            account-emojis
            ))

(define-record-type <account>
  (account-from-json json)
  account?
  (json account-json))

(set-record-type-printer! <account>
  (lambda (account port)
    (format port "#<account id: ~s acct: ~s ...>"
            (account-id account)
            (account-acct account))))

(define (account-id account)
  (assoc-ref (account-json account) "id"))

(define (account-display-name account)
  (assoc-ref (account-json account) "display_name"))

(define (account-avatar-static account)
  (assoc-ref (account-json account) "avatar_static"))

(define (account-acct account)
  (assoc-ref (account-json account) "acct"))

(define (account-emojis account)
  (map emoji-from-json
       (vector->list (assoc-ref (account-json account) "emojis"))))
