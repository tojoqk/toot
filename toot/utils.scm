;;; Toot --- Mastodon client.
;;; Copyright © 2020 Masaya Tojo <masaya@tojo.tokyo>
;;;
;;; This file is part of Toot.
;;;
;;; Toot is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; Toot is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Toot.  If not, see <http://www.gnu.org/licenses/>.

(define-module (toot utils)
  #:use-module (ice-9 format)
  #:use-module (srfi srfi-19)
  #:export (and/nil

            created-at->creation-time
            creation-time->string))


;;;
;;; Macros.
;;;

(define-syntax-rule (and/nil test expr)
  (if test
      expr
      '()))


;;;
;;; Date
;;;

(define (created-at->creation-time str)
  (time-utc->date
   (date->time-utc
    (string->date str "~Y-~m-~dT~H:~M:~S.~N~z"))))

(define (creation-time->string date)
  (date->string date "~4"))
