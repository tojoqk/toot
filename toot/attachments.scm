;;; Toot --- Mastodon client.
;;; Copyright © 2020 Masaya Tojo <masaya@tojo.tokyo>
;;;
;;; This file is part of Toot.
;;;
;;; Toot is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; Toot is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Toot.  If not, see <http://www.gnu.org/licenses/>.

(define-module (toot attachments)
  #:use-module (srfi srfi-9)
  #:export (attachment-from-json
            attachment?
            attachment-type
            attachment-preview-url
            attachment-url))

(define-record-type <attachment>
  (attachment-from-json json)
  attachment?
  (json attachment-json))

(define (attachment-type attachment)
  (string->symbol (assoc-ref (attachment-json attachment) "type")))

(define (attachment-preview-url attachment)
  (assoc-ref (attachment-json attachment) "preview_url"))

(define (attachment-url attachment)
  (assoc-ref (attachment-json attachment) "url"))
