;;; Toot --- Mastodon client.
;;; Copyright © 2020 Masaya Tojo <masaya@tojo.tokyo>
;;;
;;; This file is part of Toot.
;;;
;;; Toot is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; Toot is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Toot.  If not, see <http://www.gnu.org/licenses/>.

(define-module (toot statuses)
  #:use-module (toot emojis)
  #:use-module (toot accounts)
  #:use-module (toot attachments)
  #:use-module (srfi srfi-9)
  #:use-module (srfi srfi-9 gnu)
  #:use-module (ice-9 format)
  #:use-module (srfi srfi-19)
  #:use-module (toot utils)
  #:export (status-from-json
            status?
            status-public?
            status-sensitive?
            status-id
            status-account
            status-reblog
            status-visibility
            status-spoiler-text
            status-content
            status-emojis
            status-media-attachments
            status-in-reply-to-id
            status-in-reply-to-account-id
            status-creation-time))

(define-record-type <status>
  (status-from-json json)
  status?
  (json status-json))

(set-record-type-printer! <status>
  (lambda (status port)
    (format port
            "#<status id: ~s ...>"
            (status-id status))))

(define (status-public? status)
  (eq? 'public (status-visibility status)))

(define (status-sensitive? status)
  (assoc-ref (status-json status) "sensitive"))

(define (status-id status)
  (assoc-ref (status-json status) "id"))

(define (status-account status)
  (account-from-json (assoc-ref (status-json status) "account")))

(define (status-reblog status)
  (let ((reblog/json (assoc-ref (status-json status) "reblog")))
    (if (eq? reblog/json 'null)
        #f
        (status-from-json reblog/json))))

(define (status-visibility status)
  (string->symbol (assoc-ref (status-json status) "visibility")))

(define (status-spoiler-text status)
  (let ((s (assoc-ref (status-json status) "spoiler_text")))
    (if (zero? (string-length s))
        #f
        s)))

(define (status-content status)
  (assoc-ref (status-json status) "content"))

(define (status-emojis status)
  (map emoji-from-json
       (vector->list (assoc-ref (status-json status) "emojis"))))

(define (status-media-attachments status)
  (let ((v (assoc-ref (status-json status) "media_attachments")))
    (map attachment-from-json (vector->list v))))

(define (status-in-reply-to-id status)
  (let ((id (assoc-ref (status-json status) "in_reply_to_id")))
    (if (eq? 'null id)
        #f
        id)))

(define (status-in-reply-to-account-id status)
  (let ((id (assoc-ref (status-json status) "in_reply_to_account_id")))
    (if (eq? 'null id)
        #f
        id)))

(define (status-creation-time status)
  (created-at->creation-time (assoc-ref (status-json status)
                                        "created_at")))
